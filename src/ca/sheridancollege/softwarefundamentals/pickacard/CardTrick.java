/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.sheridancollege.softwarefundamentals.pickacard;
import java.util.Random;
import java.util.Scanner;
/**
 * A class that fills a magic hand of 7 cards with random Card Objects
 * and then asks the user to pick a card and searches the array of cards
 * for the match to the user's card. To be used as starting code in ICE 1
 * @author dancye
 */
public class CardTrick {
    
    public static void main(String[] args)
    {
        Card[] magicHand = new Card[7];
        Scanner s = new Scanner(System.in);
        Random random = new Random();
        
        for (int i=0; i<magicHand.length; i++)
        {
            Card c = new Card();
            
            c.setValue(random.nextInt(13) + 1);
            c.setSuit(Card.SUITS[random.nextInt(3)]);
            
            magicHand[i] = c;
            
            //c.setValue(insert call to random number generator here)
            //c.setSuit(Card.SUITS[insert call to random number between 0-3 here])
        }
        
        System.out.println();
        Card c1 = new Card();
        
        System.out.println();
        int card, suitNum;
        //insert code to ask the user for Card value and suit, create their card
        do
        {
            System.out.print("\nPick a card value(1-13): ");
            card = s.nextInt();
        }while((card <= 0) || (card > 13));
        
        c1.setValue(card);
        
        do
        {
            System.out.print("\nPick 0-3 for suits \"0 = Hearts\", \"1 = Diamonds\", \"2 = Spades\", \"3 = Clubs\" : ");
            suitNum = s.nextInt();    
        }while(suitNum < 0 || suitNum > 3);
        
        System.out.println();
        
        for(int j = 0; j <= Card.SUITS.length;j++)
        {
            if(suitNum == j)
            {
                c1.setSuit(Card.SUITS[j]);
            }
        }
        
        String suit = c1.getSuit();
        
        System.out.println();
        
        //Then report the result here
        for(Card hand: magicHand)
        {
            if(c1.getValue() == hand.getValue() && suit == hand.getSuit())
            {
                System.out.println("\nFound " + c1.getValue() + " of " + suit + ", in the magic hand\n");
                break;
            }
            else
            {
                System.out.printf("\nSorry! " + c1.getValue() + " of " + suit + " is not in the magic hand\n");
                break;
            }
        }
    }
    
}
